from django.contrib import admin

# import models
from .models import Category,Posts

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    readonly_fields = [
        'slug',
        'created_on',
        'modified',
    ]
class CategoryAdmin(admin.ModelAdmin):
    pass

admin.site.register(Posts, PostAdmin)
admin.site.register(Category, CategoryAdmin)
