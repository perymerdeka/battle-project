from django.db import models

# import User
from django.contrib.auth.models import User

# import slugify
from django.utils.text import slugify

# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length=30, default='uncategorized')

class Posts(models.Model):
    author = models.ForeignKey(User, on_delete= models.CASCADE,related_name='blog_posts')
    title = models.CharField(max_length=100)
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    category = models.ManyToManyField('Category' ,related_name='posts')
    slug = models.SlugField(editable=False, blank=True, unique=True)
    cover = models.ImageField(blank=True, null=True, upload_to='media_cover_posts/')

    class Meta:
        ordering = [
            '-created_on'
        ]

    def save(self):
        self.slug = slugify(self.title)
        super().save()

    def __str__(self):
        return "{} ".format(self.title)
