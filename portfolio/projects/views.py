from django.shortcuts import render

# import models
from .models  import Project

# Create your views here.

def projects_index_view(request):
    projects = Project.objects.all()
    context = {
        'projects': projects,
        'title': 'Project List Page',
    }

    return render(request, 'projects/project_index.html', context)


def projects_detail_view(request,slug):
    project = Project.objects.filter(slug=slug)

    context = {
        'project': project,
    }

    return render(request, 'projects/project_detail.html', context)
