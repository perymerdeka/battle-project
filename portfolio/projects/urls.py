from django.urls import path
from . import views

app_name = 'projects'
urlpatterns = [
    path('', views.projects_index_view, name='project_index'),
    path('page/<slug:slug>', views.projects_detail_view, name='project_detail'),
]
