from django.db import models
from django.utils.text import slugify

# Create your models here.
class Project(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    technology = models.CharField(max_length=50)
    url = models.URLField(max_length=200, blank=True)
    slug = models.SlugField(blank=True, editable=False, unique=True)
    image = models.ImageField(blank=True, null=True, upload_to='project_img/')

    def save(self):
        self.slug = slugify(self.title)
        super().save()

    def __str__(self):
        return "{}. {}".format(self.id, self.title)
