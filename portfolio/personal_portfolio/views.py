from django.shortcuts import render

def index(request):
    context = {
        'title':'Portfolio Page',
    }

    return render(request, 'index.html', context)
