from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

# import views
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('projects/', include('projects.urls', namespace='projects')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
